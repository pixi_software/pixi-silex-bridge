<?php

namespace Pixi\SilexBridge\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Pixi\API\Soap\Client as PixiApi;

class PixiApiServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
        $app['pixi.api'] = $app->share(function ($app) {
            static $initialized = false;

            if ($initialized) {
                return $app['pixi.api.connection'];
            }

            $initialized = true;

            $app['pixi.api.connection'] = new PixiApi(null, [
                "trace"    => $app['debug'] ? true : false,
                "login"    => $app['pixi.config']['api']['username'],
                "password" => $app['pixi.config']['api']['password'],
                "location" => $app['pixi.config']['api']['location'],
                "uri"      => $app['pixi.config']['api']['uri'],
                'user_agent'     => 'pixi API Client 0.1',
                'soap_version'   => SOAP_1_2,
                'ssl_method'     => SOAP_SSL_METHOD_TLS,
                'stream_context' => [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true,
                    ]
                ]
            ]);

            return $app['pixi.api.connection'];
        });
    }

    public function boot(Application $app)
    {
        //
    }

}
