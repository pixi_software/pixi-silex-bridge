<?php

namespace Pixi\SilexBridge\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;

abstract class BaseController implements ControllerProviderInterface
{
    use \Pixi\SilexBridge\AppTrait;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $db;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $customerdb;

    /**
     * @var \Pixi\API\Soap\Client
     */
    protected $pixiapi;

    public function __construct(Application $app)
    {
        $this->setApp($app);

        $this->db         = $app['dbs']['db'];
        $this->customerdb = $app['dbs']['customerdb'];

        $this->pixiapi = $app['pixi.api'];
    }

    public function connect(Application $app)
    {
        $class   = get_class($this);
        $name    = strtolower(str_replace('\\', '.', $class));
        $service = sprintf("controller.%s", $name);

        $app[$service] = $app->share(function() use ($app, $class) {
            return new $class($app);
        });

        return (object) [
            'service' => $service . ":",
            'bind'    => sprintf("%s.", $name),
        ];
    }
}
