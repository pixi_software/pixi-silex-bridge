<?php

namespace Pixi\SilexBridge;

use Pimple;

trait AppTrait {
    protected $app;

    /**
     * @param Pimple $app;
     * @return self
     */
    public function setApp(Pimple $app)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * Returns app or it's content on a specific id
     *
     * If you don't provide $default value, exception will
     * be raised by Pimple if there is nothing under $pimpleId
     *
     * @param string id
     * @param mixed  default value
     */
    public function app($pimpleId = null, $default = null)
    {
        if (is_string($pimpleId)) {
            if (!isset($this->app[$pimpleId]) && !is_null($default)) {
                return $default;
            }

            return $this->app[$pimpleId];
        }

        return $this->app;
    }
}
